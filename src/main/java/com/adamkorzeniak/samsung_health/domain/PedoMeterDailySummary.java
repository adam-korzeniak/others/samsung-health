package com.adamkorzeniak.samsung_health.domain;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class PedometerDailySummary {

    @CsvColumn(name = "create_time")
    private LocalDateTime createTime;

    @CsvColumn(name = "update_time")
    private LocalDateTime updateTime;

    @CsvColumn(name = "day_time")
    private Long date;

    @CsvColumn(name = "step_count")
    private Integer stepCount;

    @CsvColumn(name = "run_step_count")
    private Integer runStepCount;

    @CsvColumn(name = "walk_step_count")
    private Integer walkStepCount;

    @CsvColumn(name = "distance")
    private Double distanced;

    @CsvColumn(name = "calorie")
    private Double calorie;

    @CsvColumn(name = "active_time")
    private Long activeTime;

}
