package com.adamkorzeniak.samsung_health.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;


public class FloorsClimbed {
    private static String FILENAME = "com.samsung.health.floors_climbed.202111042036.csv";

    private LocalDateTime startTime;
    private LocalDateTime updateTime;
    private LocalDateTime createTime;
    private LocalDateTime endTime;
    private Double floors;

    public FloorsClimbed(Map<String, String> entry) {
        startTime = extractDate(entry.get("start_time"));
        updateTime = extractDate(entry.get("update_time"));
        createTime = extractDate(entry.get("create_time"));
        endTime = extractDate(entry.get("end_time"));
        floors = extractNumber(entry.get("floor"));
    }

    private Double extractNumber(String value) {
        return Double.parseDouble(value);
    }

    public static LocalDateTime extractDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        return LocalDateTime.parse(date, formatter);
    }
}
