package com.adamkorzeniak.samsung_health.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class Sleep {
    private static String FILENAME = "com.samsung.health.floors_climbed.202111042036.csv";

    private LocalDateTime endTime;
    private LocalDateTime updateTime;
    private  Long date;

    private Double distance;
    private Double calorie;
    private Integer count;

    public Sleep(Map<String, String> entry) {
        updateTime = extractDate(entry.get("update_time"));
        endTime = extractDate(entry.get("end_time"));
    }

    private Double extractNumber(String value) {
        return Double.parseDouble(value);
    }

    public static LocalDateTime extractDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        return LocalDateTime.parse(date, formatter);
    }
}
