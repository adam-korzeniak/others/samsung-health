package com.adamkorzeniak.samsung_health.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class CaloriesBurned {
    private static String FILENAME = "com.samsung.health.floors_climbed.202111042036.csv";

    private LocalDateTime updateTime;
    private LocalDateTime createTime;
    private Long dateTime;

    private Double restCalorie;
    private Double activeCalorie;

    public CaloriesBurned(Map<String, String> entry) {
        updateTime = extractDate(entry.get("update_time"));
        createTime = extractDate(entry.get("create_time"));
    }

    private Double extractNumber(String value) {
        return Double.parseDouble(value);
    }

    public static LocalDateTime extractDate(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
        return LocalDateTime.parse(date, formatter);
    }
}
